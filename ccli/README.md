<!-- CINCHDOC DOCUMENT(User Guide) SECTION(CLI) -->

# Cinch Command-Line Interface

Cinch provides an extensible command-line interface for adding
build system utilities, such as source file templates, and documentation
generation.  This utility is written in Python and should be installed
prior to using the Cinch CMake interface in your project.
